IMAGES=nameplate.pdf nameplate-lunch.pdf rh-logo.pdf badge.pdf
DEPS=rh-fonts.tex
CSVS=$(wildcard *.csv)

all: badges.pdf nameplates.pdf clean

%.pdf : %.tex $(IMAGES) $(CSVS) $(DEPS)
	texfot xelatex $<

clean:
	rm -rf *.log *.aux 

.PHONY: all clean
